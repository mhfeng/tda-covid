# TDA COVID-19 Project

This project contains code used for 

## Environment setup
Required packages:
* GeoPandas
* GUDHI
* NetworkX
* Pandas

## Examples

### Included examples
* Minimal workflow for taking a shapefile and getting the adjacency complex is contained in tda_covid/examples/minimal_workflow.py

### Code snippets
Use `find_neighbors` in the `shape` module to compute the neighbors (using queen adjacency) of every polygon in a shapefile.
```python
neighbors_gdf = shape.find_neighbors(covid_gdf)
```

Use `compute_adjv_persistence` in the `persistence` module to compute the adjacency filtration with a function attached to nodes of a graph with attached node data.
```python
covid_st = persistence.compute_adjv_persistence(covid_graph, col_key)
```

### Contact
If you have questions about this code, contact mhfeng@g.ucla.edu