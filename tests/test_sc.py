import gudhi as gd
import networkx as nx

from tda_covid import persistence

def test_adjv():
    G = nx.Graph()
    G.add_edges_from([(0,1),(1,2),(2,3),(0,3)])
    nx.set_node_attributes(G, {i: i for i in range(4)}, name='f_key')
    st = persistence.compute_adjv_persistence(G,'f_key')
    st.compute_persistence()
    assert isinstance(st, gd.simplex_tree.SimplexTree)
    assert st.dimension() == 2
    assert st.betti_numbers() == [1, 1]
