import geopandas as gpd

from shapely.geometry import Polygon

from tda_covid import shape

def test_find_neighbors():
    test_dict = {'name': ['A', 'B', 'C'],
                 'geometry': [Polygon([[0,0], [0,1], [1,1], [1,0]]),
                              Polygon([[0.5, 0], [0.5, 1], [1.5, 1], [1.5, 0]]),
                              Polygon([[2,2], [2,3], [3, 3], [3, 2]])]}
    test_gdf = gpd.GeoDataFrame(test_dict)
    neighbors_gdf = shape.find_neighbors(test_gdf)
    assert neighbors_gdf['Neighbors'][0] == [1]
    assert neighbors_gdf['Neighbors'][1] == [0]
    assert neighbors_gdf['Neighbors'][2] == []