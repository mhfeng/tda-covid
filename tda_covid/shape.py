import geopandas as gpd

def find_neighbors(shp_gdf):
    list_neighbors = []
    for orig_index, orig_entry in shp_gdf.iterrows():
        intersects_df = shp_gdf['geometry'].intersects(orig_entry['geometry'])
        orig_neighbors = [i for i, does_intersect in intersects_df.iteritems() if does_intersect is True]
        orig_neighbors.remove(orig_index)
        list_neighbors.append(orig_neighbors)
    shp_gdf['Neighbors'] = list_neighbors
    return shp_gdf
