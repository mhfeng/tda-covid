import geopandas as gpd
import networkx as nx

from tda_covid import examples
from tda_covid import persistence
from tda_covid import shape


if __name__ == '__main__':
    sample_shp = f'{examples.data_dir}COVID19_by_Neighborhood-shp/COVID19_by_Neighborhood.shp'
    covid_gdf = gpd.read_file(sample_shp)
    col_key = 'case_rate'

    # Calls code to find the neighbors of each area in the shapefile and put them in a new column called 'Neighbors'
    neighbors_gdf = shape.find_neighbors(covid_gdf)

    # Converts geodataframe to a networkX graph
    covid_graph = nx.Graph()
    area_edges = []
    for area_idx, area in neighbors_gdf.iterrows():
        area_edges += [(area_idx, neighbor) for neighbor in area['Neighbors']]
    covid_graph.add_edges_from(area_edges)
    nx.set_node_attributes(covid_graph, {area_idx: area[col_key] for area_idx, area in neighbors_gdf.iterrows()}, col_key)

    # Computes adjacency filtration
    covid_st = persistence.compute_adjv_persistence(covid_graph, col_key)