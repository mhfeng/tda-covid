import gudhi as gd
import networkx as nx

def compute_adjv_persistence(G, function_key, max_d = 2):
    st = gd.SimplexTree()
    H = nx.convert_node_labels_to_integers(G)
    for u in H.nodes():
        st.insert([u], filtration=int(H.nodes[u][function_key]))

    for (u,v) in H.edges():
        if u != v:
            st.insert([u, v], filtration=max(int(H.nodes[u][function_key]), int(H.nodes[v][function_key])))
        u_nb = set([n for n in H.neighbors(u)])
        v_nb = set([n for n in H.neighbors(v)])
        nbs = list(u_nb.intersection(v_nb))
        for w in nbs:
            if u != w and v != w:
                st.insert([u, v, w])
    st.make_filtration_non_decreasing()
    st.set_dimension(max_d)
    return st